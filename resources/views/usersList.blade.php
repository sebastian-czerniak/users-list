<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="css/app.css" rel="stylesheet" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="js/app.js"></script>

    </head>
    <body>
        <h1>Lista użytkowników</h1>
        <div id="users-list">
    @foreach ($users as $user)

        <div class="user" id="u-{{$user['id']}}">
            <div class="name">{{ $user['first_name'] }} {{ $user['last_name'] }}</div>
            <div class="photo"></div>
            <div class="collapse"><div class="expand">pokaż zdjęcie</div><div class="fold">ukryj zdjęcie</div></div>
            
        </div>
    @endforeach
    


    <div class="pager">
    @for ($i = 1; $i <= $totalPages; $i++)

        @if ($i == $page)
            <a class="active" href="{{ $i }}">{{ $i }}</a>
        @else
            <a href="{{ $i }}">{{ $i }}</a>
        @endif

    @endfor
    </div>
    </div>



    </body>
</html>
