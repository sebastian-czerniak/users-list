        $(document).ready(function(){

            $('.collapse').on('click', function(){

                var container = $(this).parent();
                var apiURL = 'https://reqres.in/api/users/';

                container.toggleClass('collapsed');

                if(container.hasClass('loaded'))
                {
                    return;
                }

                var userId = $(this).parent().attr('id').toString();
                var divId = userId;
                var imgContainer = $(this).parent().find('.photo');

                userId = userId.substr(2);
                var ajaxLoader = $('<img src="ajax-loader.gif" />');
                ajaxLoader.appendTo(imgContainer);

                $.get( apiURL + userId, function( data ) {

                    var downloadingImage = $("<img />");

                    downloadingImage.attr('src', data['data']['avatar']);
                    downloadingImage.on('load', function(){
                        imgContainer.empty();
                        downloadingImage.appendTo(imgContainer);
                        container.addClass('loaded');
                    });

                });
            });

        });