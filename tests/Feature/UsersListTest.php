<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersListTest extends TestCase
{


	private $apiDomain = 'https://reqres.in';

    /**
     * Check if indexes in api are available.
     *
     * @return void
     */
    public function testExample()
    {
        $url = $this->apiDomain . "/api/users?page=1";
    	$usersList = json_decode(file_get_contents($url), true);
        $this->assertArrayHasKey('data', $usersList);
        $this->assertArrayHasKey('page', $usersList);
        $this->assertArrayHasKey('total_pages', $usersList);
    }
}
