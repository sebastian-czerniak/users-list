<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

	private $apiDomain = 'https://reqres.in';

    /**
     * Check if indexes in api are available.
     *
     * @return void
     */
    public function testExample()
    {
        
        $url = $this->apiDomain . "/api/users/1";
    	$user = json_decode(file_get_contents($url), true);
        $this->assertArrayHasKey('data', $user);
        $this->assertArrayHasKey('avatar', $user['data']);

    }
}
