<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersList extends Controller
{

	private $apiDomain = 'https://reqres.in';

    /**
     *
     * Shows html page with users list
     *
     * @param int $page
     * @return mixed
     */

    public function showPage($page = 1){


    	$usersList = $this->getUsersPageInJson($page);
    	

    	return !empty($usersList['data']) ? view( 'usersList', [ 'users' => $usersList['data'], 'page' =>$usersList['page'], 'totalPages' =>$usersList['total_pages'] ] ) : view( '404' );
    }

    /**
     *
     * Return list of users in JSON
     *
     * @param $page
     * @return mixed
     */

    private function getUsersPageInJson($page){

    	$url = $this->apiDomain . "/api/users?page=" . $page;
    	$usersList = json_decode(file_get_contents($url), true);
    	
    	return $usersList;

    }
}
